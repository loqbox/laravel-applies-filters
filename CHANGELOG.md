# Changelog

All notable changes to `loqbox/partner-api-http-client` will be documented in this file.

## 0.1.10 - 2024-05-17

### Changed
- Revert IN-821

## 0.1.9 - 2024-04-25

### Changed
- IN-821: Force equals on email searches on the user relation

## 0.1.8 - 2024-04-15

### Changed
- PRT-1348: Update PHPDoc for getFilteredData

## 0.1.7 - 2022-06-09

### Changed
- DEV-220: Allow searching by a hashed value

## 0.1.6 - 2021-08-31

### Added
- WEB-132: Initial commit
