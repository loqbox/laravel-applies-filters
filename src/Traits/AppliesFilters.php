<?php

namespace Loqbox\AppliesFilters\Traits;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use stdClass;

/**
 * AppliesFilters
 *
 * This trait allows us to filter the model collection
 * based on URL parameters sent by an API query.
 *
 */
trait AppliesFilters
{
    public $query;
    public $order;
    public $filters;

    /**
     * Get filtered data
     *
     * Returns paginated data based on a set of filter parameters
     *
     * @param Request $request
     * @param string $resource
     * @param Builder $query
     * @return array
     *
     * The $resource string has a presumed root of \App\Http\Resources.
     * Any sub-namespace should be included in the string e.g. MyCustomNameSpace\MyCustomResource
     */
    public function getFilteredData(
        Request $request,
        string $resource,
        Builder $query = null
    ) {
        $this->query = $query ?? $this->query();

        if ($request->has('order')) {
            $this->applyQueryOrder($request->input('order'));
        }

        if ($request->has('filters')) {
            $this->applyQueryFilters($request->input('filters'));
        }

        if ($request->has('search')) {
            $this->applyGlobalSearch($request->input('search'));
        }

        $paginated = $this->query->paginate($request->input('page_size', 25));
        $paginated->setCollection(
            $paginated->getCollection()->map(function ($item) use ($resource) {
                $class = '\App\Http\Resources\\' . $resource;
                return (new $class($item))->resolve();
            })
        );

        return ['paginated' => $paginated->toArray()];
    }

    /**
     * Apply the ordering for the data
     *
     * @param string $order
     * @return void
     */
    public function applyQueryOrder(string $order)
    {
        $order = json_decode($order);

        $field = $order->field;

        $filterableFields = $this->getFilterableFields();
        if (array_key_exists($field, $filterableFields)) {
            $f = $filterableFields[$field];
            $field = $f['column'];
            if (is_array($field)) {
                $field = $field[0];
            }
            if ($relation = $this->getFilterRelation($f)) {
                $this->query->with([
                    $relation => function ($query) use ($field, $order) {
                        $query->orderBy($field, $order->direction);
                    },
                ]);
            } else {
                $this->query->orderBy($field, $order->direction);
            }
        } else {
            $this->query->orderBy($order->field, $order->direction);
        }
    }

    /**
     * Apply the filtering for the data
     *
     * @param array $filters - the filters upon we wish to search
     * @return void
     */
    public function applyQueryFilters(array $filters)
    {
        // Get the filterable fields
        $filterableFields = $this->getFilterableFields();

        foreach ($filters as $filter) {
            $filter = json_decode($filter);

            if (!array_key_exists($filter->field, $filterableFields)) {
                continue;
            }

            $field = $filterableFields[$filter->field];
            $this->configureFilter($filter, $field);

            if ($relation = $this->getFilterRelation($field)) {
                $this->query->whereHas($relation, function ($query) use (
                    $filter,
                    $field
                ) {
                    $this->addQuery($filter, $field, $query);
                });
            } else {
                $this->addQuery($filter, $field, $this->query);
            }
        }
    }

    /**
     * Global search fields can be defined in the model with the
     * global_searchable field within the filterable fields
     **/

    public function applyGlobalSearch(string $searchValue): void
    {
        $globalSearchables = Arr::where($this->filterableFields, function ($value) {
            return !empty($value['global_searchable']);
        });

        //Wrap global search query in a callback to allow filters to be used on the results of the search
        $this->query->where(function ($q) use ($globalSearchables, $searchValue) {

            foreach ($globalSearchables as $key => $field) {

                //Create a filter object so we can apply the same logic from the filters to each field
                $filter = new stdClass;
                $filter->column = $key;
                $filter->value = $searchValue;
                $this->configureFilter($filter, $field);

                if ($relation = $this->getFilterRelation($field)) {
                    //Ensure we apply fuzzy logic so do an OR not AND
                    $q->orWhereHas($relation, function ($qu) use (
                        $filter,
                        $field
                    ) {
                        $this->addQuery($filter, $field, $qu);
                    });
                } else {
                    $this->addOrQuery($filter, $field, $q);
                }
            }
        });
    }

    public function getFilterRelation($field)
    {
        if (isset($field['relation']) && is_string($field['relation'])) {
            return $field['relation'];
        }
        return null;
    }

    /**
     * Configure the filters
     *
     * @param object $filter
     * @param array $field
     * @return void
     */
    public function configureFilter(object &$filter, array &$field)
    {
        if (!isset($field['operator'])) {
            $field['operator'] = '=';
        }

        if (isset($field['column'])) {
            $filter->field = $field['column'];
        }

        if (isset($field['hash_search'])) {
            $filter->value = md5($filter->value);
        }

        switch ($field['operator']) {
            case 'gt':
                $filter->operator = '>';
                break;
            case 'gte':
                $filter->operator = '>=';
                break;
            case 'lt':
                $filter->operator = '<';
                break;
            case 'lte':
                $filter->operator = '<=';
                break;
            case 'starts_with':
                $filter->value = $filter->value . '%';
                $filter->operator = 'LIKE';
                break;
            case 'ends_with':
                $filter->value = '%' . $filter->value;
                $filter->operator = 'LIKE';
                break;
            case 'contains':
                $filter->value = '%' . $filter->value . '%';
                $filter->operator = 'LIKE';
                break;
            default:
                $filter->operator = '=';
        }
    }

    public function addQuery($filter, $field, $query)
    {
        if (is_array($field['column'])) {
            $query->where(function ($query) use ($filter, $field) {
                foreach ($field['column'] as $f) {
                    switch ($field['operator']) {
                        case 'between':
                            $query->orWhereBetween($f, $filter->value);

                            break;
                        default:
                            $query->orWhere(
                                $f,
                                $filter->operator,
                                $filter->value
                            );
                    }
                }
            });
        } else {
            switch ($field['operator']) {
                case 'between':
                    $query->orWhereBetween($filter->field, $filter->value);
                    break;
                default:
                    $query->where(
                        $filter->field,
                        $filter->operator,
                        $filter->value
                    );
            }
        }
    }

    /**
     * Very similar to addQuery
     * but we ensure that we are using OR for each exists (select...)
     * that we add onto the query
     */
    public function addOrQuery($filter, $field, $query) : void
    {

        if (!is_array($field['column'])) {
            switch ($field['operator']) {
                case 'between':
                    $query->orWhereBetween($filter->field, $filter->value);
                    return;
                default:
                    $query->orWhere(
                        $filter->field,
                        $filter->operator,
                        $filter->value
                    );
                    return;
            }
        }

        $query->orWhere(function ($query) use ($filter, $field) {
            foreach ($field['column'] as $f) {
                switch ($field['operator']) {
                    case 'between':
                        $query->orWhereBetween($f, $filter->value);

                        break;
                    default:
                        $query->orWhere(
                            $f,
                            $filter->operator,
                            $filter->value
                        );
                }
            }
        });
        return;
    }

    public function getFilterableFields()
    {
        $fields = $this->filterableFields;
        foreach ($fields as $key => $field) {
            if (is_string($field)) {
                $fields[$field] = [
                    'relation' => null,
                    'column' => $field,
                ];
                unset($fields[$key]);
            }
        }
        return $fields;
    }
}
